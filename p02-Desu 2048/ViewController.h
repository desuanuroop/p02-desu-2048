//
//  ViewController.h
//  p02-Desu 2048
//
//  Created by Anuroop Desu on 2/5/16.
//  Copyright © 2016 Anuroop Desu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *label1;

@property (strong, nonatomic) IBOutlet UIButton *up;

@property (strong, nonatomic) IBOutlet UIButton *right;

@property (strong, nonatomic) IBOutlet UIButton *down;

@property (strong, nonatomic) IBOutlet UIButton *left;

@property (strong, nonatomic) IBOutlet UIButton *exit;

@property (strong, nonatomic) IBOutlet UILabel *total_score;

FOUNDATION_EXPORT int changes;

-(IBAction)buttonpress:(id)sender;

- (void)addUp;
- (void)addLeft;
- (void)addRight;
- (void)addDown;
- (void)generate;
@end
