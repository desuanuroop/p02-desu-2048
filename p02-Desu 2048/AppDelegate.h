//
//  AppDelegate.h
//  p02-Desu 2048
//
//  Created by Anuroop Desu on 2/5/16.
//  Copyright © 2016 Anuroop Desu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

