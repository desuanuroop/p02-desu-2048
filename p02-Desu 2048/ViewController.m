//
//  ViewController.m
//  p02-Desu 2048
//
//  Created by Anuroop Desu on 2/5/16.
//  Copyright © 2016 Anuroop Desu. All rights reserved.
//
#import "ViewController.h"
#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@" Logging");
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@ "greybg.jpg"]];
    for (UILabel *alabel in self.label1) {
        alabel.text =@ "";
        alabel.layer.borderWidth = 1.0;
        alabel.layer.cornerRadius = 5;
        
    }
    _up.backgroundColor = Rgb2UIColor(75, 165, 255);
    _right.backgroundColor = Rgb2UIColor(75, 165, 255);
    _down.backgroundColor = Rgb2UIColor(75, 165, 255);
    _left.backgroundColor = Rgb2UIColor(75, 165, 255);
    _exit.backgroundColor = Rgb2UIColor(75, 165, 255);
    
    
    [self generate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)generate {
    
    int tile,possible=0;
    NSMutableArray *arryRandomNumber;
    arryRandomNumber=[[NSMutableArray alloc]init];
    
    while (arryRandomNumber.count<15){
        tile=arc4random_uniform(15);
        if(![((UILabel *)[self.label1 objectAtIndex:tile]).text  isEqualToString: @""]){
            if (![arryRandomNumber containsObject:[NSString stringWithFormat:@"%d",tile]])       {
                [arryRandomNumber addObject:[NSString stringWithFormat:@"%d",tile]];
            }
        }
        else{
            ((UILabel *)[self.label1 objectAtIndex:tile]).text = [NSString stringWithFormat:@ "2"];
            break;
        }
    }
    int highest_tile_score = 0;
    for(int i=0; i<16;i++){
        int tile_value = [((UILabel *)[self.label1 objectAtIndex:i]).text intValue];
        if (highest_tile_score < tile_value)
            highest_tile_score = tile_value;
    }
    _total_score.text = [NSString stringWithFormat:@ "Highest Tile: %d", highest_tile_score];

    if(arryRandomNumber.count == 15){
        
        for(int curr_cell=0; curr_cell<15;curr_cell++){
                if(curr_cell /4  != 3 ){
                    if ([((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: ((UILabel *)[self.label1 objectAtIndex:curr_cell+1]).text] || [((UILabel *)[self.label1 objectAtIndex:       curr_cell]).text  isEqualToString: ((UILabel *)[self.label1 objectAtIndex:curr_cell+4]).text])
                        possible ++;
                }
                else
                    if ([((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: ((UILabel *)[self.label1 objectAtIndex:curr_cell+1]).text])
                        possible++;
                if (possible != 0)
                    break;
        }
        if (possible == 0){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"End of the Play"
                                                           message: @"Sorry No more possible Moves"
                                                          delegate: self
                                                cancelButtonTitle:@ "Reset"
                                                 otherButtonTitles:@"Exit",nil];
            [alert show];
            
        }
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != [alertView cancelButtonIndex]) {
        exit(0);
    }
    else
        [self viewDidLoad];
}

-(IBAction)buttonpress:(UIButton *)sender{
    
    if(sender.tag == 1)
        [self addUp];
    else if (sender.tag == 2)
        [self addRight];
    else if (sender.tag == 3)
        [self addDown];
    else if (sender.tag == 4)
        [self addLeft];
    [self generate];
}

-(void)addUp {
    
    for (int i=4, start_cell = 12 ;i>0 && start_cell > 0; i--,start_cell -= 4){
        
        for (int col = 0; col < 4; col++) {
            
            int curr_cell = start_cell + col;
            int value1 = [((UILabel *)[self.label1 objectAtIndex:curr_cell]).text intValue];
            int value2 = [((UILabel *)[self.label1 objectAtIndex:curr_cell-4]).text intValue];
            
            if (value1 == value2 && ![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqual: @""]){
                ((UILabel *)[self.label1 objectAtIndex:curr_cell-4]).text = [NSString stringWithFormat:@ "%d", value1*2];
                ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text = @"";
            }
            else if (![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: @""] && [((UILabel *)[self.label1 objectAtIndex:curr_cell-4]).text  isEqualToString: @""]) {
                int tmp_cell;
                for (int z = i-2 ; z >= 0; z--) {
                    tmp_cell = z * 4 + col;
                    value2 = [((UILabel *)[self.label1 objectAtIndex:tmp_cell]).text intValue];
                    if (value1 == value2){
                        ((UILabel *)[self.label1 objectAtIndex:tmp_cell]).text = [NSString stringWithFormat:@ "%d", value1*2];
                        ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text = @"";
                    }
                }
            }
        }//End of for
    }//End of for
    
    for (int i=4, start_cell = 12 ;i>0 && start_cell >0; i--,start_cell -= 4){
        
        for (int col = 0; col < 4; col++) {
            
            int curr_cell = start_cell + col;
            
            if(![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: @""] && [((UILabel *)[self.label1 objectAtIndex:curr_cell-4]).text  isEqualToString: @""]){
                ((UILabel *)[self.label1 objectAtIndex:curr_cell-4]).text = ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text;
                ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text =@ "";
                if(start_cell != 12){
                    start_cell = 12;
                    i=5;
                    break;
                }
            }
        }
    }
} //End of method.

-(void)addDown {
    for (int i=0, start_cell = 0 ;i<4 && start_cell <12; i++,start_cell += 4){
        
        for (int col = 0; col < 4; col++) {
            
            int curr_cell = start_cell + col;
            int value1 = [((UILabel *)[self.label1 objectAtIndex:curr_cell]).text intValue];
            int value2 = [((UILabel *)[self.label1 objectAtIndex:curr_cell+4]).text intValue];
            
            if (![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqual: @""]) {
                
                if (value1 == value2) {
                    ((UILabel *)[self.label1 objectAtIndex:curr_cell+4]).text = [NSString stringWithFormat:@ "%d", value1*2];
                    ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text = @"";
                }
                else if (![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: @""] && [((UILabel *)[self.label1 objectAtIndex:curr_cell+4]).text  isEqualToString: @""]) {
                    int tmp_cell;
                    for (int z = i+2 ; z<=3; z++) {
                        tmp_cell = z * 4 + col;
                        value2 = [((UILabel *)[self.label1 objectAtIndex:tmp_cell]).text intValue];
                        if (value1 == value2){
                            ((UILabel *)[self.label1 objectAtIndex:tmp_cell]).text = [NSString stringWithFormat:@ "%d", value1*2];
                            ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text = @"";
                        }
                    }
                }
            }
        }
    }
    for (int i=4, start_cell = 0 ;i>0 && start_cell <12; i--,start_cell += 4){
        
        for (int col = 0; col < 4; col++) {
            
            int curr_cell = start_cell + col;
            
            if(![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: @""] && [((UILabel *)[self.label1 objectAtIndex:curr_cell+4]).text  isEqualToString: @""]){
                ((UILabel *)[self.label1 objectAtIndex:curr_cell+4]).text = ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text;
                ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text =@ "";
                if(start_cell != 0){
                    start_cell = 0;
                    i=5;
                    break;
                }
            }
        }
    }
}

- (void)addRight {
    for (int i=4,start_cell = 12; i>0 && start_cell >= 0; i--,start_cell -=4 ) {
        
        for (int col = 0; col < 3; col++){
            
            int curr_cell  = start_cell + col;
            int value1 = [((UILabel *)[self.label1 objectAtIndex:curr_cell]).text intValue];
            int value2 = [((UILabel *)[self.label1 objectAtIndex:curr_cell + 1]).text intValue];
            
            if (value1 == value2 && ![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqual: @""]) {
                ((UILabel *)[self.label1 objectAtIndex:curr_cell+1]).text = [NSString stringWithFormat:@ "%d", value1*2];
                ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text = @"";
            }
            else if (![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: @""] && [((UILabel *)[self.label1 objectAtIndex:curr_cell+1]).text  isEqualToString: @""]) {
                int tmp_cell;
                for (int z = col+2 ; z<=3; z++) {
                    tmp_cell = start_cell + z;
                    value2 = [((UILabel *)[self.label1 objectAtIndex:tmp_cell]).text intValue];
                    if (value1 == value2){
                        ((UILabel *)[self.label1 objectAtIndex:tmp_cell]).text = [NSString stringWithFormat:@ "%d", value1*2];
                        ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text = @"";
                    }
                }
            }
        }
    }
    
    for (int i=4, start_cell = 12 ;i>0 && start_cell >= 0; i--,start_cell -= 4){
        
        for (int col = 0; col < 3; col++) {
            
            int curr_cell = start_cell + col;
            
            if(![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: @""] && [((UILabel *)[self.label1 objectAtIndex:curr_cell+1]).text  isEqualToString: @""]){
                ((UILabel *)[self.label1 objectAtIndex:curr_cell+1]).text = ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text;
                ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text =@ "";
                if(col != 0){
                    start_cell = 16;
                    i=5;
                    break;
                }
            }
        }
    }
}

- (void)addLeft{
    for (int i=4,start_cell = 12; i>0 && start_cell >= 0; i--, start_cell -= 4){
        for (int col = 3; col > 0; col--) {
            
            int curr_cell = start_cell + col;
            int value1 = [((UILabel *)[self.label1 objectAtIndex:curr_cell]).text intValue];
            int value2 = [((UILabel *)[self.label1 objectAtIndex:curr_cell - 1]).text intValue];
            
            if (value1 == value2 && ![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqual: @""]){
                ((UILabel *)[self.label1 objectAtIndex:curr_cell-1]).text = [NSString stringWithFormat:@ "%d", value1*2];
                ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text = @"";
            }
            
            else if (![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: @""] && [((UILabel *)[self.label1 objectAtIndex:curr_cell-1]).text  isEqualToString: @""]) {
                
                for (int z = col-2 ; z>=0; z--) {
                    int tmp_cell = start_cell + z;
                    value2 = [((UILabel *)[self.label1 objectAtIndex:tmp_cell]).text intValue];
                    if (value1 == value2){
                        ((UILabel *)[self.label1 objectAtIndex:tmp_cell]).text = [NSString stringWithFormat:@ "%d", value1*2];
                        ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text = @"";
                    }
                }
                
            }
        }
    }
    
    for (int i=4, start_cell = 15 ;i>0 && start_cell > 0; i--,start_cell -= 4){
        
        for (int col = 0; col < 3; col++) {
            
            int curr_cell = start_cell - col;
            
            if(![((UILabel *)[self.label1 objectAtIndex:curr_cell]).text  isEqualToString: @""] && [((UILabel *)[self.label1 objectAtIndex:curr_cell-1]).text  isEqualToString: @""]){
                ((UILabel *)[self.label1 objectAtIndex:curr_cell-1]).text = ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text;
                ((UILabel *)[self.label1 objectAtIndex:curr_cell]).text =@ "";
                if(col != 15){
                    start_cell = 19;
                    i=5;
                    break;
                }
            }
        }
    }
}
@end